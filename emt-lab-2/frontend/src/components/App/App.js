import './App.css';
import React, {Component} from 'react';
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";
import Header from '../Header/header';
import Books from '../Books/BookList/books';
import BookEdit from '../Books/BookEdit/bookEdit';
import BookAdd from "../Books/BookAdd/bookAdd";
import Categories from "../Categories/categories";
import Authors from '../Authors/authors';
import EMTLabService from "../../repository/emtLab2Respository";

class App extends Component {

    constructor(props) {
      super(props);
      this.state = {
          books: [],
          categories: [],
          authors: [],
          selectedBook: {}
      }
    }

    render() {
        return (
            <Router>
                <Header />
                <main>
                    <div>
                        <Route path={"/authors"} exact render={() =>
                            <Authors authors={this.state.authors} />}/>
                        <Route path={"/categories"} exact render={() =>
                            <Categories categories={this.state.categories}/>}/>
                        <Route path={"/products/add"} exact render={() =>
                            <BookAdd categories={this.state.categories}
                                        authors={this.state.authors}
                                        onAddBook={this.addBook} />} />
                        <Route path={"/books/edit/:id"} exact render={() =>
                            <BookEdit categories={this.state.categories}
                                         authors={this.state.authors}
                                         onEditBook={this.editBook}
                                         book={this.state.selectedBook} />} />
                        <Route path={"/books"} exact render={() =>
                            <Books books={this.state.books}
                                   onDelete={this.deleteBook}
                                   onEdit={this.getBook}
                                   onTakeBook={this.takeBook}/>} />
                        <Redirect to={"/books"}/>
                    </div>
                </main>
            </Router>
        )
    }

    componentDidMount() {
        this.loadBooks();
        this.loadCategories();
        this.loadAuthors();
    }

    loadAuthors = () => {
        EMTLabService.fetchAuthors()
            .then((data) => {
                this.setState({
                    authors: data.data
                })
            })
    }

    loadCategories = () => {
        EMTLabService.fetchCategories()
            .then((data) => {
                this.setState({
                    categories: data.data
                })
            })
    }

    loadBooks = () => {
        EMTLabService.fetchBooks()
            .then((data) => {
                this.setState({
                    books: data.data
                })
            })
    }

    deleteBook = (id) => {
        EMTLabService.deleteBook(id)
            .then(() => {
                this.loadBooks();
            })
    }

    addBook = (name, availableCopies, category, author) => {
        EMTLabService.addBook(name, availableCopies, category, author)
            .then(() => {
                this.loadBooks();
            })
    }

    editBook = (id, name, availableCopies, category, author) => {
        EMTLabService.editBook(id, name, availableCopies, category, author)
            .then(() => {
                this.loadBooks();
            })
    }

    getBook = (id) => {
        EMTLabService.getBook(id)
            .then((data) => {
                this.setState({
                    selectedBook: data.data
                })
            })
    }

    takeBook = (id) => {
        EMTLabService.takeBook(id)
            .then(() => {
                this.loadBooks();
            })
    }
}


export default App;
