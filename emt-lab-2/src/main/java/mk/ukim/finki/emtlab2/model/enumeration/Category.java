package mk.ukim.finki.emtlab2.model.enumeration;

public enum Category {

    NOVEL, THRILLER, HISTORY, FANTASY, BIOGRAPHY, CLASSICS,

}
