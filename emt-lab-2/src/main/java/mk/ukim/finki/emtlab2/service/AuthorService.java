package mk.ukim.finki.emtlab2.service;

import mk.ukim.finki.emtlab2.model.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorService {
    Optional<Author> findById(Long id);
    List<Author> findAll();
}
